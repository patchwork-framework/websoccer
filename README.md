# Patchwork websoccer

Websockets integration for Patchwork - an asynchronous distributed microservices framework.

## Requirements
* Python 3.7

## Installation

``pip install pachwork-websoccer``

## Documentation

https://patchwork-framework.gitlab.io/core