# Overview

This module subscribes for internal patchwork messages and re-translates them for external messages which 
can be delivered for instance to web clients. As a middleware Redis pub/sub is used, each task can be
translated to multiple messages on virtual queues at Redis.

External client may subscribe using internal protocol for virtual Redis queues. Currently, only websockets
transport  is available (SSE and http poll in progress).

There is only subscription allowed now, publishing from clients to patchwork is not supported (yet).