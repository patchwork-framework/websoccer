# Patchwork Websoccer

Project source code can be found at [GitLab](https://gitlab.com/patchwork-framework/websoccer).

Package name: `patchwork-websoccer`

This package contains an implementation of patchwork messages to web clients integration.
